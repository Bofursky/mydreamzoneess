package net.myotherworld.Ess.Command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import net.myotherworld.Ess.Ess;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpCommand implements CommandExecutor
{
	private Ess plugin;
	
	public HelpCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
	    }
		if(sender.hasPermission("MyOtherWorldEssentials.help")) 
		{	
			Player p = (Player)sender;
			if(args.length == 0)
			{
				File tempFile = new File(plugin.getDataFolder() + File.separator + "Help", "Help.txt");
				
				try(BufferedReader br = new BufferedReader(new FileReader(tempFile))) 
				{
				    for(String line; (line = br.readLine()) != null; ) 
				    {
				    	p.sendMessage(ChatColor.translateAlternateColorCodes('&', line));				    
				    }
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			if(args.length == 1)
			{	
				String arg = args[0];
				String txt = arg.replace(args[0], args[0]+ ".txt");
						
				File tempFile = new File(plugin.getDataFolder() + File.separator + "Help",txt );
			    if(tempFile.exists())
			    {
			    	try(BufferedReader br = new BufferedReader(new FileReader(tempFile))) 
					{
					    for(String line; (line = br.readLine()) != null; ) 
					    {
					    	p.sendMessage(ChatColor.translateAlternateColorCodes('&', line));				    
					    }
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
			    }				
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}