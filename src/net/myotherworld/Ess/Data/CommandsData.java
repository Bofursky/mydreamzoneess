package net.myotherworld.Ess.Data;

import java.util.LinkedHashMap;
import java.util.List;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

public class CommandsData 
{
	public Ess plugin;
	public List<String> Commands;
	public String Permission;
	
	public LinkedHashMap<String, List<String>> Cmd = new LinkedHashMap<String, List<String>>();
	public LinkedHashMap<String, String> Pex = new LinkedHashMap<String, String>();
	
	public CommandsData(Ess plugin)
	{
		this.plugin = plugin;	
	}	
	public CommandsData(YamlConfiguration data) 
	{		
		ConfigurationSection c = data.getConfigurationSection("");
		if (c == null)
		{
			Bukkit.getLogger().severe("Usun Commands.yml i przeladuj ponownie");
			return;
		}
		for (String key : c.getKeys(false))
		{
			Commands = data.getStringList(key + ".Commands");												
			Permission = data.getString(key + ".Permission");
			
			Cmd.put(key, Commands);
			Pex.put(key, Permission);
		}
	}
}
