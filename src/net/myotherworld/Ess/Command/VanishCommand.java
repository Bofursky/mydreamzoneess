package net.myotherworld.Ess.Command;

import java.util.ArrayList;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VanishCommand implements CommandExecutor
{
	public static  ArrayList<Player> vanished = new ArrayList<Player>();
	
	private Ess plugin;
	public VanishCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
	    }
		if(sender.hasPermission("MyOtherWorldEssentials.vanish")) 
		{			
			Player p = (Player)sender;
		
			if (!vanished.contains(p)) 
			{
				for (Player pl : Bukkit.getServer().getOnlinePlayers()) 
				{
					pl.hidePlayer(p);
			
				}
					vanished.add(p);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.VanishOn);
					return true;
			}
			else 
			{		
				for (Player pl : Bukkit.getServer().getOnlinePlayers()) 
				{
					pl.showPlayer(p);					
				}
					vanished.remove(p);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.VanishOff);
					return true;
			}	
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}