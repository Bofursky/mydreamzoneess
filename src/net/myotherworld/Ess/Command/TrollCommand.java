package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TrollCommand implements CommandExecutor 
{
	private Ess plugin;
	public TrollCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
    	if(sender.hasPermission("MyOtherWorldEssentials.troll")) 
    	{
			if(args.length < 4)
			{
				sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Troll);
				return true;
			}	
        	Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
    		if (targetPlayer == null)
    		{
    			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[0]);
    			return true;
    		}
			if(args.length == 4)
			{
				try 
				{		
					int X = Integer.parseInt(args[1].toString());
					int Y = Integer.parseInt(args[2].toString());
					int Z = Integer.parseInt(args[3].toString());								
					World world = targetPlayer.getWorld();
					targetPlayer.teleport(new Location(world, X, Y, Z));
					targetPlayer.playSound(targetPlayer.getLocation(), Sound.GHAST_SCREAM, 100, 0 ); 
					targetPlayer.playSound(targetPlayer.getLocation(), Sound.WOLF_DEATH, 100, 0 );
	        	} 
				catch (IllegalArgumentException e) 
				{
					sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Troll);					
				}

				return true;							
			}
			if(args.length == 5)
			{
				try 
				{		
					int X = Integer.parseInt(args[1].toString());
					int Y = Integer.parseInt(args[2].toString());
					int Z = Integer.parseInt(args[3].toString());								
					World world = Bukkit.getWorld(args[4].toString());
					if(world != null)
					{
						
						targetPlayer.teleport(new Location(world, X, Y, Z));
						targetPlayer.playSound(targetPlayer.getLocation(), Sound.GHAST_SCREAM, 100, 0 ); 
						targetPlayer.playSound(targetPlayer.getLocation(), Sound.WOLF_DEATH, 100, 0 );
					}
					else
					{
						sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.WorldInvalid);
					}
					
	        	} 
				catch (IllegalArgumentException e) 
				{
					sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Troll);					
				}
				return true;							
			}
    	}
    	else
    	{
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
    	}
		return true;
    }
}