package net.myotherworld.Ess.Command;

import java.util.ArrayList;
import java.util.List;

import net.myotherworld.Ess.Ess;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class NearCommand implements CommandExecutor
{
	private Ess plugin;
	public NearCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{		
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}	
		if(sender.hasPermission("MyOtherWorldEssentials.near")) 
		{	
			if (args.length == 0) 
			{      			
				Player player = (Player)sender;
			    List<Player> players = new ArrayList<Player>();
			    int distance = 200;
			    for (Entity entity : player.getNearbyEntities(distance, distance, distance))
			    {
			      if ((entity instanceof Player)) 
			      {
			        players.add((Player)entity);
			      }
			    }
			    if (players.size() < 1)
			    {
			      sender.sendMessage("Brak");
			      return true;
			    }
			    sender.sendMessage("Gracze:");
			    for (Player a : players)
			    {
			    	int i = (int)a.getLocation().distance(player.getLocation());
			        sender.sendMessage(ChatColor.RED + a.getDisplayName() + ChatColor.GRAY + " (" + ChatColor.GRAY + i + ChatColor.GRAY + ")");	     
			    }
			    return true;
			}
		}		
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}