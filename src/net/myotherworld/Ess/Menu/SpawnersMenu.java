package net.myotherworld.Ess.Menu;

import java.util.ArrayList;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class SpawnersMenu
{
	private Ess plugin;
	private Material material;
	private Integer amount;
	
	public SpawnersMenu(Ess plugin)
	{
		this.plugin = plugin;
	}
	public void Menu(Player player) 
	{
		material = Material.MONSTER_EGG;
		amount = 1;
		
		Inventory inv = Bukkit.createInventory(null, 36, "�6Lista dostepnych mobow");
	
		player.playSound(player.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);
	    
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.bat") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.bat")))
	    {		    
		    Byte data = 65;
			String name = "�2Bat";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.bat"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceBat);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.blaze") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.blaze")))
	    {
		    Byte data = 61;
			String name = "�2Blaze";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.blaze"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceBlaze);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.cavespider") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.cavespider")))
	    {
		    Byte data = 59;
			String name = "�2Cave Spider";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.cavespider"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceCaveSpider);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
			
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.chicken") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.chicken")))
	    {
		    Byte data = 93;
			String name = "�2Chicken";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.chicken"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceChicken);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));			
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.cow") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.cow")))
	    {
		    Byte data = 92;
			String name = "�2Cow";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.cow"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceCow);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));			
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.crepper") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.crepper")))
	    {
		    Byte data = 50;
			String name = "�2Crepper";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.crepper"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceCrepper);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));			
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.enderdragon") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.enderdragon")))
	    {
		    Byte data = 63;
			String name = "�2Ender Dragon";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.enderdragon"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceEnderDragon);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));				
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.enderman") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.enderman")))
	    {
		    Byte data = 58;
			String name = "�2Enderman";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.enderman"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceEnderman);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));				
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.ghast") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.ghast")))
	    {
		    Byte data = 56;
			String name = "�2Ghast";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.ghast"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceGhast);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));	
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.giant") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.giant")))
	    {
		    Byte data = 53;
			String name = "�2Giant";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.giant"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceGiant);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));				
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.guardian") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.guardian")))
	    {	  
	    	Byte data = 68;
			String name = "�2Guardian";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.guardian"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceGuardian);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));		
	    }	   
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.irongolem") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.irongolem")))
	    {
	    	Byte data = 99;
			String name = "�2Iron Golem";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.irongolem"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceIronGolem);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
			inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }	
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.magmacube") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.magmacube")))
	    {
	    	Byte data = 62;
			String name = "�2Magma Cube";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.magmacube"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceMagmaCube);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }	    
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.mushroomcow") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.mushroomcow")))
	    {
	    	Byte data = 96;
			String name = "�2Mushroom Cow";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.mushroomcow"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceMushroomCow);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.ocelot") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.ocelot")))
	    {
	    	Byte data = 95;
			String name = "�2Ocelot";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.ocelot"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceOcelot);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.pig") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.pig")))
	    {
	    	Byte data = 90;
			String name = "�2Pig";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.pig"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PricePig);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.pigzombie") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.pigzombie")))
	    {
	    	Byte data = 57;
			String name = "�2Pig Zombie";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.pigzombie"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PricePigZombie);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.rabbit") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.rabbit")))
	    {
	    	Byte data = 101;
			String name = "�2Rabbit";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.rabbit"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceRabbit);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.sheep") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.sheep")))
	    {
	    	Byte data = 91;
			String name = "�2Sheep";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.sheep"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceSheep);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.silverfish") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.silverfish")))
	    {
	    	Byte data = 60;
			String name = "�2SilverFish";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.silverfish"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceSilverFish);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.skeleton") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.skeleton")))
	    {
	    	Byte data = 91;
			String name = "�2Skeleton";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.skeleton"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceSkeleton);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.slime") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.slime")))
	    {
	    	Byte data = 55;
			String name = "�2Slime";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.slime"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceSlime);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.snowman") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.snowman")))
	    {
	    	Byte data = 97;
			String name = "�2SnowMan";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.snowman"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceSnowMan);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.spider") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.spider")))
	    {
	    	Byte data = 52;
			String name = "�2Spider";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.spider"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceSpider);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.squid") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.squid")))
	    {
	    	Byte data = 94;
			String name = "�2Squid";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.squid"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceSquid);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }  	    
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.villager") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.villager")))
	    {
	    	Byte data = 120;
			String name = "�2Villager";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.villager"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceVillager);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.witch") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.witch")))
	    {
	    	Byte data = 66;
			String name = "�2Witch";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.witch"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceWitch);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    } 
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.wither") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.wither")))
	    {
	    	Byte data = 38;
			String name = "�2Wither";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.wither"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceWither);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    } 
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.wolf") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.wolf")))
	    {
	    	Byte data = 95;
			String name = "�2Wolf";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.wolf"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceWolf);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    } 
	    if(player.hasPermission("MyOtherWorldEssentials.spawners.zombie") || (player.hasPermission("MyOtherWorldEssentials.spawners.free.zombie")))
	    {
	    	Byte data = 54;
			String name = "�2Zombie";
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.zombie"))
		    {
		    	lore.add(plugin.messageData.Price + plugin.configData.PriceZombie);
		    }
		    else
		    {
		    	lore.add(ChatColor.GOLD + "Free");
		    }
		    inv.addItem(plugin.itemManager.createItem(material, amount, data, name, lore));
	    }	    
	    player.openInventory(inv);
	}

}
