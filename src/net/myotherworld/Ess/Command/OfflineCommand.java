package net.myotherworld.Ess.Command;

import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.PlayerInteractManager;
import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.mojang.authlib.GameProfile;

public class OfflineCommand implements CommandExecutor
{
	private Ess plugin;
	public OfflineCommand(Ess plugin)
	{
		this.plugin = plugin;
	}		
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
    	Player p = (Player)sender;
    	
		if(sender.hasPermission("MyOtherWorldEssentials.Admin")) 
		{
    		if(args.length == 0)
    		{
    			sender.sendMessage("Wpisz ender lub inv");
    			return true;
    		}
    		if(args.length > 0)
    		{
	    		if (args[0].equalsIgnoreCase("Ender"))
				{
	        		if(args.length == 2)
	        		{
		    			@SuppressWarnings("deprecation")
						OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
						GameProfile gameprofile = new GameProfile(player.getUniqueId(), player.getName());
						MinecraftServer minecraftserver = MinecraftServer.getServer();
						EntityPlayer entity = new EntityPlayer(minecraftserver, minecraftserver.getWorldServer(0), gameprofile, new PlayerInteractManager(minecraftserver.getWorldServer(0)));
						final Player target = entity == null ? null : entity.getBukkitEntity();					
						if (target != null)
						{
						   	target.loadData();
							Inventory TargetEnch = target.getEnderChest();
							p.openInventory(TargetEnch); 	
						}  						
						target.saveData();
						return true;
	        		}
	        		else
	        		{
	        			sender.sendMessage("Wpisz ender gracz");
	        			return true;
	        		}
						
				}  	    		
	    		else if (args[0].equalsIgnoreCase("Inv"))
	    		{  
	        		if(args.length == 2)
	        		{
		    			@SuppressWarnings("deprecation")
						OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
						GameProfile gameprofile = new GameProfile(player.getUniqueId(), player.getName());
						MinecraftServer minecraftserver = MinecraftServer.getServer();
						EntityPlayer entity = new EntityPlayer(minecraftserver, minecraftserver.getWorldServer(0), gameprofile, new PlayerInteractManager(minecraftserver.getWorldServer(0)));
						final Player target = entity == null ? null : entity.getBukkitEntity();
						if (target != null)
						{
							target.loadData();	
							Inventory Inv = target.getInventory();
							p.openInventory(Inv);
						}  			
						target.saveData();
						return true;
	        		}
	        		else
	        		{
	        			sender.sendMessage("Wpisz ender gracz");
	        			return true;
	        		}
	    		}  		
    		}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}