package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RepairCommand implements CommandExecutor 
{
	private Ess plugin;
	public RepairCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
    	if(sender.hasPermission("MyOtherWorldEssentials.repair")) 
    	{	
			Player player = (Player) sender;		
			ItemStack item = player.getItemInHand();		
			@SuppressWarnings("deprecation")
			Material material = Material.getMaterial(item.getTypeId());
			
			if (item == null || material.isBlock() || material.getMaxDurability() < 1)
			{
				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Repair);
			}
			else
			{
				player.getInventory().getItemInHand().setDurability((short) 0);
				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.RepairPass);
			}	
    	}
	    else
        {
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
        }	 	
    		
	return true;
    }    	
}
