package net.myotherworld.Ess.Command;


import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GMCommand implements CommandExecutor
{
	private Ess plugin;
	public GMCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		Player p = (Player)sender;
	
		if(sender.hasPermission("MyOtherWorldEssentials.gm")) 
		{				
			if(args.length == 0)
			{
				p.sendMessage(plugin.messageData.Prefix + plugin.messageData.GM);
				return true;
			}				
			if(args.length == 1)
			{
				if(args[0].equalsIgnoreCase("1"))
				{
					p.setGameMode(GameMode.CREATIVE);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.GMOn);
				}
				else if(args[0].equalsIgnoreCase("0"))
				{
					p.setGameMode(GameMode.SURVIVAL);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.GMOff);
				}
				else
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.GM);
					return true;
				}
			}						
			else if ((args.length == 2) && (p.hasPermission("MyOtherWorldEssentials.gm.others")))
			{
				Player targetPlayer = Bukkit.getServer().getPlayer(args[1]);	
                if (targetPlayer == null)
                {
                    p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[1]);
                    return true;
                }
				if(args[0].equalsIgnoreCase("1"))
				{
					targetPlayer.setGameMode(GameMode.CREATIVE);
					targetPlayer.sendMessage(plugin.messageData.Prefix + plugin.messageData.GMOn);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.GMOn + " dla " + targetPlayer.getName());
				}
				else if(args[0].equalsIgnoreCase("0"))
				{
					targetPlayer.setGameMode(GameMode.SURVIVAL);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.GMOff + " dla " + targetPlayer.getName());
					targetPlayer.sendMessage(plugin.messageData.Prefix + plugin.messageData.GMOff);
				}
				else
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.GM);
					return true;
				}
			}
		}
	    else
	    {
	    	sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
	    }
		return true;
	}
	
}
