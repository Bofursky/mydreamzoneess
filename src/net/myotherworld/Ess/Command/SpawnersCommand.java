package net.myotherworld.Ess.Command;

import java.util.HashSet;

import net.myotherworld.Ess.Ess;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnersCommand implements CommandExecutor
{
	private Ess plugin;
	public SpawnersCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.spawner")) 
		{
			Player player = (Player) sender;
			@SuppressWarnings("deprecation")
			Block spawner = player.getTargetBlock((HashSet<Byte>) null, 10);		
			if (args.length == 0) 
			{			
				if (spawner == null) 
				{
					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerLook);
					return true;
				}
				else if (!spawner.getType().equals(Material.MOB_SPAWNER)) 
				{
					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpanwerWrong);
					return true;
				}
				else if (spawner.getType().equals(Material.MOB_SPAWNER)) 
				{
					plugin.spawnersMenu.Menu(player);
					return true;
				}
			}
			else
			{
				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerLook);
			}					
		}
	    else
	    {
	    	sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
	    }
		return true;
	}
}
