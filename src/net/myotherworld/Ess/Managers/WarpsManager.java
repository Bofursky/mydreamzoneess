package net.myotherworld.Ess.Managers;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

public class WarpsManager 
{
	private WarpsManager() {}
        static WarpsManager instance = new WarpsManager();
       
        public static WarpsManager getInstance() 
        {
        	return instance;
        }
        Plugin p;
             
        FileConfiguration Warps;
        File Wfile;
       
	public void setup(Plugin p) 
	{
               
		if (!p.getDataFolder().exists()) 
		{
			p.getDataFolder().mkdir();
		}
		Wfile = new File(p.getDataFolder(), "Warps.yml");
               
		if (!Wfile.exists()) 
		{
			try 
			{
				Wfile.createNewFile();
			}
			catch (IOException e) 
			{
				Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create Warps.yml!");
			}
		}
               
		Warps = YamlConfiguration.loadConfiguration(Wfile);
	}
       
	public FileConfiguration getData() 
	{
		return Warps;
	}
       
	public void saveData() 
	{
		try 
		{
			Warps.save(Wfile);
		}
		catch (IOException e) 
		{
			Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save Warps.yml!");
		}
	}
	public void reloadData() 
	{
		Warps = YamlConfiguration.loadConfiguration(Wfile);
	}    
	public PluginDescriptionFile getDesc() 
	{
	return p.getDescription();
	}
}