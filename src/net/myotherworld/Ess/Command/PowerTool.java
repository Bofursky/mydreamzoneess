package net.myotherworld.Ess.Command;

import java.util.ArrayList;

import net.myotherworld.Ess.Ess;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PowerTool implements CommandExecutor
{
	private Ess plugin;
	public PowerTool(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}	
		Player p = (Player)sender;
		if(sender.hasPermission("MyOtherWorldEssentials.powertol")) 
		{	
			if (args.length >= 1)
			{
				ItemStack IS = p.getItemInHand();
                if (IS.getType() == Material.STICK)
                {
                	String command = "";
                	for (int i = 1; i < args.length; i++) 
                    {
                		command = command + " " + args[i];
                    }
                    ItemMeta Meta = IS.getItemMeta();
                    Meta.setDisplayName("�cPowertool");
                    ArrayList<String> lore = new ArrayList<String>();
                    lore.add("�a/" + args[0] + command);
                    Meta.setLore(lore);
                    IS.setItemMeta(Meta);
                    sender.sendMessage("Poprawnie dodano komende");
                }
                else
                {
                	sender.sendMessage("Uzyj na patyku");
                }
			}
			else
			{
				sender.sendMessage("/powertool <cmd>");
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}