package net.myotherworld.Ess.Menu;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class AdminMenu 
{
	private Ess plugin;
	
	public AdminMenu(Ess plugin)
	{
		this.plugin = plugin;
	}
	public void Menu(Player p) 
	{	
		Inventory inv = Bukkit.createInventory(null, 9, plugin.messageData.Server);
		
	    p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);
	    
	    if(p.hasPermission("MyOtherWorldEssentials.message"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "Reload message";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.config"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "Reload config";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }	   
	    p.openInventory(inv);
	}
}
