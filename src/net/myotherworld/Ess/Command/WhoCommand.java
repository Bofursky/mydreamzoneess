package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WhoCommand implements CommandExecutor
{
	private Ess plugin;
	public WhoCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args)
	{	
		if(sender.hasPermission("MyOtherWorldEssentials.who")) 
		{  
			if (!(sender instanceof Player)) 
			{
				sender.sendMessage("Tylko gracz moze tego uzyc!");
				return true;
			}			
			if(args.length == 0)
			{
				Player p = (Player) sender;
				sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.WhoisInfo);
				WhoCommand.getwho(p, sender);
			}
			if(args.length > 1)
			{
				Player p = (Player) sender;
				sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.WhoisInvalid);
				WhoCommand.getwho(p, sender);
			}
			if(args.length == 1)
			{
				for(Player p : Bukkit.getOnlinePlayers()) 
				{
					if(p.getDisplayName().contains(args[0]) || p.getName().contains(args[0]))
					{ 	
						WhoCommand.getwho(p, sender);							
					}
				}		
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}	
	public static boolean getwho(Player p, CommandSender sender)
	{	 	        
		if(p != null)
		{	
			sender.sendMessage(ChatColor.DARK_RED + "Nazwa gracza:");
			sender.sendMessage("----------------------------");
			sender.sendMessage(ChatColor.GREEN + "Gracz: " + ChatColor.DARK_RED + p.getName());      
			sender.sendMessage(ChatColor.GREEN + "Nazwa Gracza: " + ChatColor.RESET + p.getDisplayName());
			sender.sendMessage("----------------------------");           
		}				
		return true;
	}	
}
