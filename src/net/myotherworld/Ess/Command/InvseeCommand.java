package net.myotherworld.Ess.Command;


import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class InvseeCommand implements CommandExecutor 
{
	private Ess plugin;
	public InvseeCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.invsee")) 
		{			
			Player p = (Player)sender;
			if (args.length == 0)
			{	
				sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.InvseeInvalid);
			}
			if (args.length == 1)
			{	 
				Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
				if (targetPlayer == null)
				{			       
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[0]);
					return true;
				}
				else if(args.length == 1)
				{		
					Inventory targetInv = targetPlayer.getInventory();
					p.openInventory(targetInv);
				}
				else
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.InvseeInvalid);
					return true;
				}
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}