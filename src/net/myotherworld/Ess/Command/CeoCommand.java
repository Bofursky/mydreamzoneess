package net.myotherworld.Ess.Command;

import java.util.HashMap;
import java.util.Map.Entry;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CeoCommand implements CommandExecutor 
{
	private Ess plugin;
	public CeoCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if(sender.hasPermission("MyOtherWorldEssentials.ceo")) 
		{
			sender.sendMessage(plugin.messageData.Ceo);
			HashMap<String,String> tempData = new HashMap<String,String>();
		
			for (Player player : Bukkit.getOnlinePlayers()) 
			{    
				if (!VanishCommand.vanished.contains(player))
				{
					if(player.hasPermission("MyOtherWorldEssentials.ceo.user")) 
					{
						tempData.put(player.getName(),plugin.vaultManager.permission.getPrimaryGroup(player));
					}
				}			
			}
			for(String group : plugin.vaultManager.permission.getGroups())
			{
				if(tempData.containsValue(group))
				{
					String message = ChatColor.GOLD + group + ChatColor.WHITE + ": ";
					for(Entry<String, String> data : tempData.entrySet())
					{
						if(data.getValue().equals(group))
						{
							//Dodajemy
							message = message + data.getKey() + ", ";
						}
					}
				sender.sendMessage(message);
				}
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}