package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

public class HealingCommand implements CommandExecutor 
{
	private Ess plugin;
	public HealingCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
	    }
		if(sender.hasPermission("MyOtherWorldEssentials.healing")) 
		{			
			Player p = (Player)sender;
		
			if(args.length == 0)
			{
				for (PotionEffect effect : p.getActivePotionEffects())
					p.removePotionEffect(effect.getType());
					p.setHealth(20.0);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Heal);
					return true;
			}
			else if((args.length == 1) && (p.hasPermission("MyOtherWorldEssentials.healing.other")))
			{						 	
				Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);	
				if (targetPlayer == null)
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[0]);
                	return true;
				}
				else if(p.getServer().getPlayer(args[0]) != null) 			
				{				
					for (PotionEffect effect : targetPlayer.getActivePotionEffects())
						targetPlayer.removePotionEffect(effect.getType());
						targetPlayer.setHealth(20.0);
						targetPlayer.sendMessage(plugin.messageData.Prefix + plugin.messageData.Heal);
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.HealOther + args[0]);
					return true;
				}
				return true;
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}