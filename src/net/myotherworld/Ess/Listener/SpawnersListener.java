package net.myotherworld.Ess.Listener;

import java.util.HashSet;

import net.milkbowl.vault.economy.EconomyResponse;
import net.myotherworld.Ess.Ess;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class SpawnersListener implements Listener
{
	private Ess plugin;
	
	public SpawnersListener(Ess plugin)
	{
		this.plugin = plugin;
	}
	@EventHandler
	public void Spawner(BlockBreakEvent event)
	{
		Player player = event.getPlayer();
		if(plugin.configData.GiveSpawner == true && player.hasPermission("MyOtherWorldEssentials.spawners.GiveSpawner"))
		{
			if(event.getBlock().getType().equals(Material.MOB_SPAWNER))
			{
				if(event.getPlayer().getItemInHand().getType() == Material.DIAMOND_PICKAXE)
				{
					if(event.getPlayer().getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH))
					{					
						ItemStack IS = new ItemStack(Material.MOB_SPAWNER);
						player.getInventory().addItem(IS);
					}
				}
			}
		}
	}
	@EventHandler
	public void Click(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;		
    	}
    	if (event.getInventory().getName().equalsIgnoreCase("�6Lista dostepnych mobow"))
    	{
    		try
    		{	
    			Player player = (Player) event.getWhoClicked();
    			@SuppressWarnings("deprecation")
				Block spawner = player.getTargetBlock((HashSet<Byte>) null, 10);	
    			if(spawner.getType().equals(Material.MOB_SPAWNER))
    			{
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Bat"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.bat")) 
	    				{
	    					EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceBat);   			
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.BAT);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.BAT );    	
		    				}
		    				else 
		    				{
		    					player.closeInventory();
		    					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.BAT);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.BAT );  			
	    				}
	    	        }    			
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Blaze"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.blaze")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceBlaze);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.BLAZE);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.BLAZE );    	
		    				}
		    				else 
		    				{
		    					player.closeInventory();
		    					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.BLAZE);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.BLAZE );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Cave Spider"))
	    	        {	    				
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.cavespider")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceCaveSpider);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.CAVE_SPIDER);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.CAVE_SPIDER );  
		    				}
		    				else 
		    				{
		    					player.closeInventory();
		    					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.CAVE_SPIDER);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.CAVE_SPIDER );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Chicken"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.chicken")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceChicken);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.CHICKEN);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.CHICKEN );
		    				}
		    				else 
		    				{
		    					player.closeInventory();
		    					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.CHICKEN);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.CHICKEN );  			
	    				}		    				
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Cow"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.cow")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceCow);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.COW);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.COW );
		    				}
		    				else 
		    				{
		    					player.closeInventory();
		    					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.COW);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.COW );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Crepper"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.crepper")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceCrepper);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.CREEPER);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.CREEPER );
		    				}
		    				else 
		    				{
		    					player.closeInventory();
		    					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.CREEPER);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.CREEPER );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Ender Dragon"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.enderdragon")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceEnderDragon);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.ENDER_DRAGON);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.ENDER_DRAGON );    
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.ENDER_DRAGON);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.ENDER_DRAGON );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Enderman"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.enderman")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceEnderman);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.ENDERMAN);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.ENDERMAN );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}	
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.ENDERMAN);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.ENDERMAN );  			
	    				}
	    	        }    			
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Ghast"))
	    	        {
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.ghast")) 
	    				{
		    				event.setCancelled(true);
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceGhast);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.GHAST);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.GHAST );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.GHAST);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.GHAST );  			
	    				}	    					    				
	    	        }  
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Giant"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.giant")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceGiant);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.GIANT);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.GIANT );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.GIANT);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.GIANT );  			
	    				}   				
	    	        }  
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Guardian"))
	    	        {
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.guardian")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceGuardian);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.GUARDIAN);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.GUARDIAN );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.GUARDIAN);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.GUARDIAN );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Iron Golem"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.irongolem")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceIronGolem);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.IRON_GOLEM);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.IRON_GOLEM );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.IRON_GOLEM);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.IRON_GOLEM );  			
	    				}		    				
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Magma Cube"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.magmacube")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceMagmaCube);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.MAGMA_CUBE);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.MAGMA_CUBE );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}	
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.MAGMA_CUBE);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.MAGMA_CUBE );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Mushroom Cow"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.mushroomcow")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceMushroomCow);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.MUSHROOM_COW);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.MUSHROOM_COW );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			} 
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.MUSHROOM_COW);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.MUSHROOM_COW );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Ocelot"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.ocelot")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceOcelot);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.OCELOT);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.OCELOT );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.OCELOT);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.OCELOT );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Pig"))
	    	        {
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.pig")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PricePig);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.PIG);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.PIG );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.PIG);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.PIG );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Pig Zombie"))
	    	        {
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.pigzombie")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PricePigZombie);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.PIG_ZOMBIE);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.PIG_ZOMBIE );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.PIG_ZOMBIE);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.PIG_ZOMBIE );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Rabbit"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.rabbit")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceRabbit);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.RABBIT);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.RABBIT );   
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.RABBIT);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.RABBIT );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Sheep"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.sheep")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceSheep);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.SHEEP);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SHEEP); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			} 
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.SHEEP);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SHEEP );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2SilverFish"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.silverfish")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceSilverFish);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.SILVERFISH);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SILVERFISH); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.SILVERFISH);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SILVERFISH );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Skeleton"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.skeleton")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceSkeleton);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.SKELETON);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SKELETON); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.SKELETON);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SKELETON );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Slime"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.slime")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceSlime);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.SLIME);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SLIME); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			} 
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.SLIME);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SLIME );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2SnowMan"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.snowman")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceSnowMan);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.SNOWMAN);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SNOWMAN); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.SNOWMAN);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SNOWMAN );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Spider"))
	    	        {
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.spider")) 
	    				{	    				
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceSpider);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.SPIDER);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SPIDER); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.SPIDER);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SPIDER );  			
	    				}	
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Squid"))
	    	        {
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.squid")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceSquid);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.SQUID);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SQUID); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.SQUID);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.SQUID );  			
	    				}	
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Villager"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.villager")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceVillager);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.VILLAGER);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.VILLAGER); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.VILLAGER);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.VILLAGER );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Witch"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.witch")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceWitch);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.WITCH);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.WITCH); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.WITCH);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.WITCH );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Wither"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.witcher")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceWither);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.WITHER);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.WITHER); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.WITHER);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.WITHER );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Wolf"))
	    	        {	
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.wolf")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceWolf);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.WOLF);
			    				s.update();
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.WOLF); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.WOLF);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.WOLF );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�2Zombie"))
	    	        {
	    				if(!player.hasPermission("MyOtherWorldEssentials.spawners.free.zombie")) 
	    				{
		    				EconomyResponse r = plugin.vaultManager.economy.withdrawPlayer(player, plugin.configData.PriceZombie);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
			    				s.setSpawnedType(EntityType.ZOMBIE);
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.ZOMBIE); 
		    				}
			   				else 
			    			{
			    				player.closeInventory();
			    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) spawner.getState();
		    				s.setSpawnedType(EntityType.ZOMBIE);
		    				s.update();
		    				player.closeInventory();
		    				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpawnerSet + EntityType.ZOMBIE );  			
	    				}
	    	        }
    			}
    		}
    	      catch (Exception localException) {}
    	}
	}
}
