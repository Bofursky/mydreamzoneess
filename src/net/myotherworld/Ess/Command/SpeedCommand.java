package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpeedCommand implements CommandExecutor
{
	private Ess plugin;
	public SpeedCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.speed")) 
		{				
			Player p = (Player) sender;		
			if (args.length == 1) 
			{
				if(p.isFlying() == true)
				{
					try 
					{
		        	    int speed = Integer.parseInt(args[0]);
		        	    p.setFlySpeed((float)speed / 10.0f);
		        	    p.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpeedInfo + args[0]);
		        	} 
					catch (IllegalArgumentException e) 
					{
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpeedInvalid + args[0]);
					}
				}
				else if(p.isFlying() == false)
				{
					try 
					{
						int speed = Integer.parseInt(args[0]);
						p.setWalkSpeed((float)speed / 10.0f);
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpeedInfo + args[0]);
					} 
					catch (IllegalArgumentException e) 
					{
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.SpeedInvalid + args[0]);
					}
				}
			}
			else
			{
				p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Speed);
				return true;
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;			
	}
}