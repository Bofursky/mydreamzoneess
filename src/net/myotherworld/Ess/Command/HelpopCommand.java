package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpopCommand implements CommandExecutor
{
	private Ess plugin;
	public HelpopCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
	    }
		if(sender.hasPermission("MyOtherWorldEssentials.helpop")) 
		{			
			Player p = (Player)sender;
			if (args.length >= 1) 
			{  
				String msg = "";
				for (int i = 0; i < args.length; i++) 
				{
					msg = msg + args[i] + " ";
				}            
				p.sendMessage(plugin.messageData.HelpOP +  msg);
				Bukkit.broadcast(plugin.messageData.HelpOP + p.getName()+":" + " " + plugin.messageData.HelpOPColor + msg, "MyOtherWorldEssentials.helpop.chat");
            
				for (Player player : Bukkit.getOnlinePlayers()) 
				{    
					if(player.hasPermission("MyOtherWorldEssentials.helpop.Sound"))
					{
						player.playSound(player.getLocation(), Sound.GHAST_SCREAM, 100, 0 ); 
					}
				}         	
			}
			else if (args.length == 0) 
			{
				p.sendMessage(plugin.messageData.HelpOP + plugin.messageData.HelpOPMsg);
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
    }
}
