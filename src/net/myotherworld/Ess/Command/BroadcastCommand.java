package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BroadcastCommand implements CommandExecutor
{
	private Ess plugin;
	public BroadcastCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
		if(sender.hasPermission("MyOtherWorldEssentials.broadcast")) 
    	{	
    		if (args.length >= 1)
    		{      		
    			String msg = "";
    			for (int i = 0; i < args.length; i++) 
    			{
    				msg = msg + args[i] + " ";
    			} 
    			Bukkit.broadcastMessage(plugin.messageData.Broadcast + plugin.messageData.BroadcastColorMsg + ChatColor.translateAlternateColorCodes('&', msg) );
    		}
    		else if(args.length == 0)			
    		{
    			sender.sendMessage(plugin.messageData.Broadcast + plugin.messageData.BroadcastMsg);
    			return true;
    		}		
    	}
	    else
        {
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
        }	    		
	return true;
    }
}