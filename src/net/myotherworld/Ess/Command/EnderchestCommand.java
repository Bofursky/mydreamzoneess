package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class EnderchestCommand implements CommandExecutor
{
	private Ess plugin;
	public EnderchestCommand(Ess plugin)
	{
		this.plugin = plugin;
	}		
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
    	if(sender.hasPermission("MyOtherWorldEssentials.enderchest")) 
    	{	
    		Player p = (Player) sender;
    		if (args.length == 0) 
    		{
    			p.openInventory(p.getEnderChest());
    		}
    		else if ((args.length == 1) && (p.hasPermission("MyOtherWorldEssentials.enderchest.others")))
    		{
    			Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
    			if (targetPlayer == null)
    			{
    				p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[0]);
					return true;
    			}
    			else if(args.length == 1)
    			{		
    				Inventory TargetEnch = targetPlayer.getEnderChest();
    				p.openInventory(TargetEnch);
    			}
    		}
    		return true;
    	}
    	else
        {
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
        }
		return true; 	    	    		
	}	
}