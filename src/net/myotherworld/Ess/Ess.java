package net.myotherworld.Ess;

import net.myotherworld.Ess.Command.AdmCommand;
import net.myotherworld.Ess.Command.AdminCommand;
import net.myotherworld.Ess.Command.BroadcastCommand;
import net.myotherworld.Ess.Command.CeoCommand;
import net.myotherworld.Ess.Command.ChatCommand;
import net.myotherworld.Ess.Command.ClearChatCommad;
import net.myotherworld.Ess.Command.EnderchestCommand;
import net.myotherworld.Ess.Command.FeedCommand;
import net.myotherworld.Ess.Command.FlyCommand;
import net.myotherworld.Ess.Command.FullCommand;
import net.myotherworld.Ess.Command.GMCommand;
import net.myotherworld.Ess.Command.HatCommand;
import net.myotherworld.Ess.Command.HealingCommand;
import net.myotherworld.Ess.Command.HelpCommand;
import net.myotherworld.Ess.Command.HelpopCommand;
import net.myotherworld.Ess.Command.InvseeCommand;
import net.myotherworld.Ess.Command.ItemCommand;
import net.myotherworld.Ess.Command.ListCommand;
import net.myotherworld.Ess.Command.MenuCommand;
import net.myotherworld.Ess.Command.MoreCommand;
import net.myotherworld.Ess.Command.NearCommand;
import net.myotherworld.Ess.Command.OfflineCommand;
import net.myotherworld.Ess.Command.PlayerTimeCommand;
import net.myotherworld.Ess.Command.PowerTool;
import net.myotherworld.Ess.Command.RepairCommand;
import net.myotherworld.Ess.Command.SkullCommand;
import net.myotherworld.Ess.Command.SpawnersCommand;
import net.myotherworld.Ess.Command.SpeedCommand;
import net.myotherworld.Ess.Command.TellCommand;
import net.myotherworld.Ess.Command.TimeCommand;
import net.myotherworld.Ess.Command.TrollCommand;
import net.myotherworld.Ess.Command.VanishCommand;
import net.myotherworld.Ess.Command.WarpsCommand;
import net.myotherworld.Ess.Command.WeatherCommand;
import net.myotherworld.Ess.Command.WhoCommand;
import net.myotherworld.Ess.Command.WhoisCommand;
import net.myotherworld.Ess.Command.WorkbenchCommand;
import net.myotherworld.Ess.Command.YoutuberBroadcastCommand;
import net.myotherworld.Ess.Data.CommandsData;
import net.myotherworld.Ess.Data.ConfigData;
import net.myotherworld.Ess.Data.MessageData;
import net.myotherworld.Ess.Listener.PlayerListener;
import net.myotherworld.Ess.Listener.SpawnersListener;
import net.myotherworld.Ess.Managers.FileManager;
import net.myotherworld.Ess.Managers.ItemManager;
import net.myotherworld.Ess.Managers.MotdManager;
import net.myotherworld.Ess.Managers.VaultManager;
import net.myotherworld.Ess.Managers.WarpsManager;
import net.myotherworld.Ess.Menu.AdminMenu;
import net.myotherworld.Ess.Menu.MainMenu;
import net.myotherworld.Ess.Menu.SpawnersMenu;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Ess extends JavaPlugin
{
	public FileManager fileManager;
	public ItemManager itemManager;
	public MessageData messageData;
    public Ess plugin;
    public MotdManager motdManager;
	public CommandsData commandsData;
	public MainMenu mainMenu;
	public AdminMenu adminMenu;
	public SpawnersMenu spawnersMenu;
	public ConfigData configData;
	public VaultManager vaultManager;
	
    private void enablelisteners()
    {
    	PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerListener(this), this);
        pm.registerEvents(new SpawnersListener(this), this);
    }         
	public void enableCommands()
	{
		getCommand("adm").setExecutor( new AdmCommand(this) );
		getCommand("MowEss").setExecutor( new AdminCommand(this) );
		getCommand("Broadcast").setExecutor( new BroadcastCommand(this) );
        getCommand("Ceo").setExecutor( new CeoCommand(this) );
        getCommand("Chat").setExecutor( new ChatCommand(this) );           
        getCommand("Clearchat").setExecutor( new ClearChatCommad(this) );
        getCommand("Enderchest").setExecutor( new EnderchestCommand(this) );
        getCommand("Feed").setExecutor( new FeedCommand(this) );
        getCommand("Fly").setExecutor( new FlyCommand(this) ); 
        getCommand("Full").setExecutor( new FullCommand(this) ); 
        getCommand("Gamemode").setExecutor( new GMCommand(this) );
        getCommand("Hat").setExecutor( new HatCommand(this) ); 
        getCommand("Heal").setExecutor( new HealingCommand(this) );
        getCommand("Help").setExecutor( new HelpCommand(this) );   
        getCommand("Helpop").setExecutor( new HelpopCommand(this) );
        getCommand("Invsee").setExecutor( new InvseeCommand(this) );
        getCommand("Item").setExecutor( new ItemCommand(this) );            
        getCommand("List").setExecutor( new ListCommand(this) );
        getCommand("Ess").setExecutor( new MenuCommand(this) );
        getCommand("More").setExecutor( new MoreCommand(this) );
        getCommand("Near").setExecutor( new NearCommand(this) );
        getCommand("Offline").setExecutor( new OfflineCommand(this) );
        getCommand("Dynamictime").setExecutor( new PlayerTimeCommand(this) );
        getCommand("Statictime").setExecutor( new PlayerTimeCommand(this) );
        getCommand("Powertool").setExecutor( new PowerTool(this) );      
        getCommand("Repair").setExecutor( new RepairCommand(this) );
        getCommand("Skull").setExecutor( new SkullCommand(this) );
        getCommand("Spawner").setExecutor(new SpawnersCommand(this) );
        getCommand("Speed").setExecutor(new SpeedCommand(this) );  
        getCommand("Tell").setExecutor( new TellCommand(this) );
        getCommand("Day").setExecutor( new TimeCommand(this) );
        getCommand("Night").setExecutor( new TimeCommand(this) );
        getCommand("Warp").setExecutor( new WarpsCommand(this) );
        getCommand("Delwarp").setExecutor( new WarpsCommand(this) );
        getCommand("Setwarp").setExecutor( new WarpsCommand(this) );
        getCommand("Sun").setExecutor( new WeatherCommand(this) );
        getCommand("Rain").setExecutor( new WeatherCommand(this) );
        getCommand("Thunder").setExecutor( new WeatherCommand(this) );
        getCommand("Troll").setExecutor( new TrollCommand(this) );
        getCommand("Vanish").setExecutor( new VanishCommand(this) );
        getCommand("Who").setExecutor( new WhoCommand(this) );
        getCommand("Whois").setExecutor( new WhoisCommand(this) );
        getCommand("Workbench").setExecutor( new WorkbenchCommand(this) );
        getCommand("Youtuber").setExecutor( new YoutuberBroadcastCommand(this) );	
	}
	@Override
	@EventHandler(priority=EventPriority.NORMAL)
    public void onEnable() 
    {  
		enableSettings();   
        enablelisteners();
        enableCommands();
        
        WarpsManager settings = WarpsManager.getInstance();
        settings.setup(this);
    }
	public void enableSettings()
	{
		//Manager
		fileManager = new FileManager(this);
		motdManager = new MotdManager(this);
		itemManager = new ItemManager();
		vaultManager = new VaultManager();
		
		//Data
		messageData = new MessageData(fileManager.loadMessages());	
		commandsData = new CommandsData(fileManager.loadCommands());
		configData = new ConfigData(fileManager.loadConfig());
		
		//Menu		
		mainMenu = new MainMenu(this);
		adminMenu = new AdminMenu(this);
		spawnersMenu = new SpawnersMenu(this);
	}
}