package net.myotherworld.Ess.Managers;

import java.io.File;
import java.io.IOException;

import net.myotherworld.Ess.Ess;

import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager 
{
	public Ess plugin;
	public File configFile;
			
	public FileManager(Ess plugin)
	{
		this.plugin = plugin;
		createFolder();
		createFile();
	}
	public YamlConfiguration loadMessages()
	{
		try 
        {
            if (!plugin.getDataFolder().exists())
            {
            	plugin.getDataFolder().mkdirs();
            }
            if (!new File(plugin.getDataFolder(), "messages.yml").exists())
            {
            	plugin.saveResource("messages.yml", false);
            }
            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "messages.yml"));           
        } 
		catch (Exception ex) 
        {
            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
            plugin.getServer().getPluginManager().disablePlugin(plugin);
            return null;
        }
	}
	public YamlConfiguration loadCommands()
	{
		try 
        {
            if (!plugin.getDataFolder().exists())
            {
            	plugin.getDataFolder().mkdirs();
            }
            if (!new File(plugin.getDataFolder(), "Commands.yml").exists())
            {
            	plugin.saveResource("Commands.yml", false);
            }
            return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "Commands.yml"));           
        } 
		catch (Exception ex) 
        {
            plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
            plugin.getServer().getPluginManager().disablePlugin(plugin);
            return null;
        }
	}
	public YamlConfiguration loadConfig()
	{
		try 
	    {
			if (!plugin.getDataFolder().exists())
	        {
				plugin.getDataFolder().mkdirs();
	        }
	        if (!new File(plugin.getDataFolder(), "config.yml").exists())
	        {
	        	plugin.saveResource("config.yml", false);
	        }
	        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "config.yml"));     
	    } 
		catch (Exception ex) 
	    {
	        plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	        plugin.getServer().getPluginManager().disablePlugin(plugin);
	        return null;
	    }
	}
	public void createFolder()
	{
		if (!plugin.getDataFolder().exists())
		{
        	plugin.getDataFolder().mkdirs();
        }
		try
		{
			configFile = new File(plugin.getDataFolder() + File.separator + "Help");
	        if(!configFile.exists())
	        {
	        	configFile.mkdirs();
	        }
		}
		catch(Exception e)
		{
			plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
		}
	}
	public void createFile()
	{
		configFile = plugin.getDataFolder();
	    if(!configFile.exists())
	    {
	    	configFile.mkdir();
	    }
	    File saveTo = new File(plugin.getDataFolder() + File.separator + "Help", "Help.txt");
	    if(!saveTo.exists())
	    {
	    	try 
	    	{
	    		saveTo.createNewFile();
	    	} 
	    	catch (IOException e) 
	    	{
	    		e.printStackTrace();
	    	}
	    }		
	}

}
