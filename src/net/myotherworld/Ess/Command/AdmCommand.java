package net.myotherworld.Ess.Command;

import java.util.List;
import java.util.Map.Entry;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AdmCommand implements CommandExecutor
{
	private Ess plugin;
	public AdmCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}		
    	if(sender.hasPermission("MyOtherWorldEssentials.adm")) 
    	{	
    		Player p = (Player)sender;
			if(args.length == 0)
			{
				for(String m : plugin.messageData.Commands) 
				{
					m = ChatColor.translateAlternateColorCodes('&', m);
					sender.sendMessage(plugin.messageData.Prefix + m);						
				}
				return true;
			}
			for (Entry<String, String> Pex : plugin.commandsData.Pex.entrySet())
			{
				if(args[0].equalsIgnoreCase((String)Pex.getKey()))
				{
					if(p.hasPermission((String)Pex.getValue()))
					{
						for (Entry<String, List<String>> Cmd : plugin.commandsData.Cmd.entrySet())
						{
							if(args[0].equalsIgnoreCase((String)Cmd.getKey()))
							{
								List<String> list = Cmd.getValue();										
								for(String commands : list)
								{			
									if (args.length > 1)
									{
									if(args[1] != null) 
									commands = commands.replace("$0", args[1]);		
									}
									if(commands.contains("CONSOLE:"))
									{
										commands = commands.replace("CONSOLE:", "");
										Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), commands);
									}
									else p.performCommand(commands);
								}
							}	
						}
					}
					else
					{
						sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);	
						return true;
					}		
				}	
			}
    	}
    	else
    	{
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
    	}
		return true;
	
	}
}
