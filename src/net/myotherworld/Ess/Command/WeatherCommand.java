package net.myotherworld.Ess.Command;


import net.myotherworld.Ess.Ess;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WeatherCommand implements CommandExecutor
{
	private Ess plugin;
	public WeatherCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    { 	
    	if (!(sender instanceof Player)) 
    	{
    		sender.sendMessage("Tylko gracz moze tego uzyc!");
    		return true;
	    }
    	Player p = (Player)sender;
    	
		if(sender.hasPermission("MyOtherWorldEssentials.sun")) 
		{   	
			if (cmd.getName().equalsIgnoreCase("Sun") )
			{   		
				p.getLocation().getWorld().setStorm(false);
    			p.getLocation().getWorld().setThundering(false);
    			p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Sun);
    			return true;
			}
		}
		if(sender.hasPermission("MyOtherWorldEssentials.rain")) 
		{  
			if (cmd.getName().equalsIgnoreCase("Rain") )
			{
    			p.getLocation().getWorld().setStorm(true);
    			p.getLocation().getWorld().setThundering(false);
    			p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Rain);
    			return true;
			}
		}		
		if(sender.hasPermission("MyOtherWorldEssentials.thunder")) 
		{  
			if (cmd.getName().equalsIgnoreCase("Thunder") )
    		{
    			p.getLocation().getWorld().setStorm(true);
    			p.getLocation().getWorld().setThundering(true);
    			p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Thunder);
    			return true;
    		}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}			
		return true;
    }
}
