package net.myotherworld.Ess.Command;


import net.myotherworld.Ess.Ess;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemCommand implements CommandExecutor 
{
	private Ess plugin;
	public ItemCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.item")) 
		{					
			Player p = (Player) sender;
			if(args.length == 0)
			{
		    	p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Item);
			}
			else if(args.length == 1)
			{
				try 
				{
					String[] idAndData = args[0].split(":");		
					int id = Integer.parseInt(idAndData[0]);	
				
					if (idAndData.length == 1) 
					{
						//item 35
						@SuppressWarnings("deprecation")
						ItemStack IS = new ItemStack(id, 1);
						p.getInventory().addItem(IS);
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Items + args[0]);	
					}
					else
					{
						//item 35:10
						int meta =  Integer.parseInt(idAndData[1]);
						@SuppressWarnings("deprecation")
						ItemStack IS = new ItemStack(id, 1, (short) meta);
						p.getInventory().addItem(IS);
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Items + args[0]);	
					}				
				}
				catch (IllegalArgumentException e) 
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.ItemDurability);
				}	
			}
			else if(args.length == 2)
			{
				try 
				{
					String[] idAndData = args[0].split(":");		
					int id = Integer.parseInt(idAndData[0]);				
							
					if (idAndData.length == 1) 
					{
						//item 35 10
						@SuppressWarnings("deprecation")
						ItemStack IS = new ItemStack(id,Integer.parseInt(args[1]));
						p.getInventory().addItem(IS);
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Items+ args[1] + " " + args[0]);
					}
					else
					{
						//item 35:10 10
						int meta =  Integer.parseInt(idAndData[1]);
						@SuppressWarnings("deprecation")
						ItemStack IS = new ItemStack(id,Integer.parseInt(args[1]), (short) meta);
						p.getInventory().addItem(IS);
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Items + args[1] + " " + args[0]);
					}			
				}	
				catch (IllegalArgumentException e) 
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.ItemAmout);
				}	
			}	
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}