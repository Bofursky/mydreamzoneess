package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand implements CommandExecutor
{
	private Ess plugin;
	public FlyCommand(Ess plugin)
	{
		this.plugin = plugin;
	}		
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.fly")) 
		{	 			
			Player p = (Player)sender;
			
			if(args.length == 0)
			{
				p.setAllowFlight(!p.getAllowFlight());
				p.sendMessage(plugin.messageData.Prefix + (p.getAllowFlight() ? plugin.messageData.FlyOn : plugin.messageData.FlyOff));
				return true;
			}	
			else if ((args.length == 1) && (p.hasPermission("MyOtherWorldEssentials.fly.off")))
			{
				if(args[0].equalsIgnoreCase("off"))
				{
					p.setAllowFlight(false);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.FlyOff);
					return true;
				}
				else if ((args.length == 1) && (p.hasPermission("MyOtherWorldEssentials.fly.others")))
				{
					Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
					if (targetPlayer == null)
					{
						p.sendMessage(plugin.messageData.PlayerInvalid + args[0]);
						return true;
					}
					if(args.length == 1)
					{
						targetPlayer.setAllowFlight(!targetPlayer.getAllowFlight());
						targetPlayer.sendMessage(plugin.messageData.Prefix + (targetPlayer.getAllowFlight() ? plugin.messageData.FlyOn : plugin.messageData.FlyOff));
						p.sendMessage(plugin.messageData.Prefix + (targetPlayer.getAllowFlight() ? plugin.messageData.FlyOn : plugin.messageData.FlyOff) + " dla " + targetPlayer.getName());
						return true;
					}			
				}
			}
		}  	
	    else
	    {
	    	sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
	    }
		return true;	    			    	
	}
}