package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FeedCommand implements CommandExecutor
{
	private Ess plugin;
	public FeedCommand(Ess plugin)
	{
		this.plugin = plugin;
	}			
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
    	if(sender.hasPermission("MyOtherWorldEssentials.feed")) 
    	{	          
    		Player p = (Player) sender;
    		if(args.length == 0)
    		{
                p.setFoodLevel(20);
                p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Feed);
                return true;
    		}
    		else if ((args.length == 1) && (p.hasPermission("MyOtherWorldEssentials.feed.others")))
    		{					   		 	
    			Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);	
                if (targetPlayer == null)
                {
                    p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[0]);
                    return true;
                }
                else if(p.getServer().getPlayer(args[0]) != null) 			
    			{	
                	targetPlayer.setFoodLevel(20);
                	targetPlayer.sendMessage(plugin.messageData.Prefix + plugin.messageData.Feed);
                    p.sendMessage(plugin.messageData.Prefix + targetPlayer.getName() + plugin.messageData.FeedOther);
    			}
    			return true;
    		}
    	}
        else
        {
        	sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
        }
    		return true;
	}
}