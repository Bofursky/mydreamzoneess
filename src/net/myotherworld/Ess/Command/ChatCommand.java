package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatCommand implements CommandExecutor 
{
	public static boolean chatDisabled;
	
	private Ess plugin;
	public ChatCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.chat")) 
		{
			if(args.length == 1)
			{
				if (args[0].equalsIgnoreCase("off")) 
				{
					chatDisabled = true;
					Bukkit.broadcastMessage(plugin.messageData.Prefix + plugin.messageData.ChatOff);
					return true;
				}
				if (args[0].equalsIgnoreCase("On")) 
				{
					chatDisabled = false;
					Bukkit.broadcastMessage(plugin.messageData.Prefix + plugin.messageData.ChatOn);
					return true;
				}
			}
			else 
			{
				sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Chat);
				return true;
			}
			return true;
		}
	    else
	    {
	    	sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
	    }
		return true;	
	}
}