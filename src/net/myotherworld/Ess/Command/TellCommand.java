package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TellCommand implements CommandExecutor
{
	private Ess plugin;
	public TellCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
    {
		if(sender.hasPermission("MyOtherWorldEssentials.tell")) 
		{	    	
	        if (args.length > 1)
	        {
	        	Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
	        	if (targetPlayer == null)
	        	{
	        		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[0]);
	        		return true;
	        	}
	        	String msg = "";
	        	for (int i = 1; i < args.length; i++) 
	        	{
	        		msg = msg + args[i] + " ";
	        	}
	        	targetPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&',  plugin.messageData.Prefix + msg));
	        }
	        else if(args.length == 1)
	        {
	        	sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Tell);
	        }
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;	     
    }
}