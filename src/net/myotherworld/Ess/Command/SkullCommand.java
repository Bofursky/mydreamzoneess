package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullCommand implements CommandExecutor
{
	private Ess plugin;
	public SkullCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
    	if(sender.hasPermission("MyOtherWorldEssentials.skull")) 
    	{	
    		Player player = (Player) sender;
    		if (args.length == 0) 
    		{
    			ItemStack skull = new ItemStack(397, 1, (short) 3);
    			SkullMeta meta = (SkullMeta) skull.getItemMeta();
    			meta.setOwner(player.getName());
    			skull.setItemMeta(meta);
    			player.getInventory().addItem(skull);
    			player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Skull);	
    			return true;
    			
    		}
    		if (args.length <= 1) 
    		{			
    			ItemStack skull = new ItemStack(397, 1, (short) 3);
    			SkullMeta meta = (SkullMeta) skull.getItemMeta();
    			meta.setOwner((args[0]));
    			skull.setItemMeta(meta);
    			player.getInventory().addItem(skull);
    			player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Skull +": " + args[0]);
    			return true;
    		}
    		else if ((args.length == 2) && (player.hasPermission("MyOtherWorldEssentials.skull.others")))
    		{	
				Player targetPlayer = Bukkit.getServer().getPlayer(args[1]);	
                if (targetPlayer == null)
                {
                    player.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalid + args[1]);
                    return true;
                }
                else
                {
	    			ItemStack skull = new ItemStack(397, 1, (short) 3);
	    			SkullMeta meta = (SkullMeta) skull.getItemMeta();
	    			meta.setOwner((args[0]));
	    			skull.setItemMeta(meta);
	    			targetPlayer.getInventory().addItem(skull);
	    			player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Skull +": " + args[0]);
	    			player.sendMessage(plugin.messageData.Prefix + targetPlayer.getName()+ plugin.messageData.SkullOther +": " + args[0]);
                }
    		}
    		
    	}
    	else
        {
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
        }
		return true;		
		

		
	}
}
