package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class YoutuberBroadcastCommand implements CommandExecutor
{
	private Ess plugin;
	public YoutuberBroadcastCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
    	if(sender.hasPermission("MyOtherWorldEssentials.youtuber")) 
    	{	
    		if (args.length >= 1)
    		{      		
    			String msg = "";
    			for (int i = 0; i < args.length; i++) 
    			{
    				msg = msg + args[i] + " ";
    			} 
    			Bukkit.broadcastMessage(plugin.messageData.BroadcastYT + plugin.messageData.BroadcastColorMsg + msg );
    		}
    		else if(args.length == 0)			
    		{
    			sender.sendMessage(plugin.messageData.BroadcastYT + plugin.messageData.BroadcastMsgYT);
    			return true;
    		}		
    	}
	    else
        {
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
        }
		return true;	
    }
} 		