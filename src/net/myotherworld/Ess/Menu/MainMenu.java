package net.myotherworld.Ess.Menu;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class MainMenu 
{
	private Ess plugin;
	
	public MainMenu(Ess plugin)
	{
		this.plugin = plugin;
	}
	public void Menu(Player p) 
	{	
		Inventory inv = Bukkit.createInventory(null, 45, plugin.messageData.Prefix);
		
	    p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);
	    
	    if(p.hasPermission("MyOtherWorldEssentials.admin"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/mowess";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.broadcast"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.BroadcastMsg;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.ceo"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/ceo";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.chat"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.Chat;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.clearchat"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/clearchat";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.enderchest"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/enderchest <player>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.feed"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/feed <player>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.fly"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.Fly;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.full"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.Full;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.gm"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.GM;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.hat"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/hat <player>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.healing"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/heal <player>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.help"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/help";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.helpop"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/helpop <wiadomosc>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.invsee"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/invsee <player>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.item"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.Item;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.list"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/list";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.more"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/more";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.near"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/near";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.stime"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.PlayerTimeS;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.dtime"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.PlayerTimeD;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.powertol"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/powertol <cmd>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.repair"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/repair";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.skull"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/skull <player>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.spawner"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/spawner";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.speed"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.Speed;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.tell"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.Tell;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.day"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/day <all>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.night"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/night <all>";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.troll"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.Troll;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.vanish"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/vanish";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.setwarp"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/setwarp [warp]";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.warp"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/warp [warp]";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.delwarp"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/delwarp [warp]";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.sun"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/sun";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.rain"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/rain";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.thunder"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/thunder";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.who"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/who [player]";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.whois"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/whois [player]";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.workbench"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = ChatColor.GREEN + "/workbench";
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    if(p.hasPermission("MyOtherWorldEssentials.youtuber"))
	    {
	    	Material material = Material.PAPER;
	    	int amount = 1;
	    	String name = plugin.messageData.BroadcastMsgYT;
	    	
	    	inv.addItem(plugin.itemManager.createItem(material, amount, name));
	    }
	    p.openInventory(inv);
	}
}
