package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearChatCommad implements CommandExecutor
{
	private Ess plugin;
	public ClearChatCommad(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
    	if(sender.hasPermission("MyOtherWorldEssentials.clearchat")) 
    	{
    		for(Player player : Bukkit.getOnlinePlayers())
    		{  
	    			for(int i=0; i < 100; i ++)
	    			{	
	    				player.sendMessage("");
	    			}   	    	
    		}
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.ClearChat);    		
    	}
	    else
        {
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
        }	   	
	return true;
    }
}