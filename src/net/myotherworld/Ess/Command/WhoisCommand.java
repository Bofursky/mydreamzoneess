package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WhoisCommand implements CommandExecutor
{
	private Ess plugin;
	public WhoisCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args)
	{			
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.whois")) 
		{  
			Player p = (Player) sender;
			switch(args.length)
			{
				case 0:
					sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.WhoisInfo );
					break;
				case 1:
					p = sender.getServer().getPlayer(args[0]);
					break;
				default:
					sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.WhoisInvalid );
			} 
			if(p != null)
			{
				this.getip(p, sender);     
	        } 
			else
			{
				sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.WhoisOfflnie );
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;	 	 
	}	
	public String getip(Player player, CommandSender sender)
    {	 
        String playerIp = player.getAddress().getHostString();
			if(player != null)
			{	
				sender.sendMessage(ChatColor.DARK_RED + "Panel Kontrolny MyOtherWorld");
				sender.sendMessage("----------------------------");
				sender.sendMessage(ChatColor.GREEN + "Gracz: " + ChatColor.DARK_RED + player.getName());      
				sender.sendMessage(ChatColor.GREEN + "Nazwa Gracza: " + ChatColor.RESET + player.getDisplayName());
				sender.sendMessage(ChatColor.GREEN + "Grupa: " + ChatColor.GOLD + plugin.vaultManager.permission.getPrimaryGroup(player));  
				sender.sendMessage(plugin.messageData.Whois + playerIp);
				sender.sendMessage(ChatColor.GREEN + "Latanie: " + ChatColor.GOLD + (player.getAllowFlight() ? "On" : "Off"));   
				sender.sendMessage(ChatColor.GREEN + "Tryb: " + player.getGameMode());  
				sender.sendMessage(ChatColor.GREEN + "OP: " + ChatColor.DARK_RED + (player.isOp() ? "Tak" : "Nie")); 
				sender.sendMessage(ChatColor.GREEN + "UUID: " + ChatColor.DARK_RED + player.getUniqueId().toString());
				sender.sendMessage("----------------------------");           
			}				
		return playerIp;
    }
}

