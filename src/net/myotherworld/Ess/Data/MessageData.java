package net.myotherworld.Ess.Data;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.ChatColor;

public class MessageData
{	
	public String Server;
	public String Prefix;
	
	public String Join;
	public String Quit;
	
	public String BroadcastMsg;
	public String Broadcast;
	
	public String Ceo;
		
	public String ChatOff;
	public String ChatOn;
	public String Chat;
	public String ChatInfo;
	
	public String ClearChat;
	
	public String Feed;
	public String FeedOther;
	
	public String Fly;
	public String FlyOn;
	public String FlyOff;
	
	public String Full;
	public String FullWrong;
	public String FullGive;
	public String FullLook;
	
	public String GM;
	public String GMOn;
	public String GMOff;
	
	public String Hat;
	public String HatEmpty;
	
	public String Heal;
	public String HealOther;
	
	public String InvseeInvalid;
	
	public String Skull;
	public String SkullOther;
	
	public String More;
	
	public String HelpOP;
	public String HelpOPColor;
	public String HelpOPMsg;
	
	public String ItemAmout;
	public String Item;
	public String ItemDurabilit;
	public String Items;
	public String ItemDurability;
	
	public String DynamicTime;
	public String StaticTime;
	public String PlayerInvalidTime;
	public String PlayerTimeS;
	public String PlayerTimeD;
	
	public String RepairPass;
	public String Repair;
	
	public String Spawner;
	public String SpawnerInvalid;
	public String SpawnerLook;
	public String SpanwerWrong;
	public String SpawnerSet;
	
	public String Speed;
	public String SpeedInvalid;
	public String SpeedInfo;
	
	public String Tell;
	
	public String Day;
	public String DayAll;
	public String Night;
	public String NightAll;	
	public String TimeColor;
	
	public String Troll;
	
	public String VanishOn;
	public String VanishOff;
	
	public String Warps;
	public String WarpsInvalid;
	public String WarpsTP;
	public String WarpsSet;
	public String WarpsRemove;
	public String WarpsInfo;
	
	public String Sun;
	public String Rain;
	public String Thunder;
	
	public String WhoisInfo;
	public String WhoisInvalid;
	public String WhoisOfflnie;
	public String Whois;
	
	public String BroadcastColorMsg;
	public String BroadcastYT;
	public String BroadcastMsgYT;
	
	public String Permissions;
	public String WorldInvalid;
	public String PlayerInvalid;
	
	public List<String> Commands = new ArrayList<String>();
	
	public String Price;
	public String Money;
	
	public MessageData(YamlConfiguration message)
	{
	    Prefix = ChatColor.translateAlternateColorCodes('&',message.getString("Prefix","Prefix "));
	    Server = message.getString("Server","Server");	
	    
	    Join = ChatColor.translateAlternateColorCodes('&',message.getString("Motd.Join", ""));
	    Quit = ChatColor.translateAlternateColorCodes('&',message.getString("Motd.Quit", ""));
	    	   	  
	    Broadcast = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Broadcast" ,"Broadcast" ));
	    BroadcastMsg = ChatColor.translateAlternateColorCodes('&',message.getString("Message.BroadcastMsg" , "Uzyj /bc [wiadomsoc]"));
	    
	    Ceo = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Ceo" , "Administracja Online: "));
	    
	    Chat = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Chat" ,"Uzyj /chat [Off] lub /chat [On]"));
	    ChatOff = ChatColor.translateAlternateColorCodes('&',message.getString("Message.ChatOff" ,"Czat zostal wylaczony"));
	    ChatOn = ChatColor.translateAlternateColorCodes('&',message.getString("Message.ChatOn" ,"Czat zostal wlaczony"));
	    ChatInfo = ChatColor.translateAlternateColorCodes('&',message.getString("Message.ChatInfo" ,"Czat zostal wylaczony"));
	    
	    ClearChat = ChatColor.translateAlternateColorCodes('&',message.getString("Message.ClearChat" ,"Czat zostal wyczyszczony"));
	    
	    Feed = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Feed" , "Zostales nakarmiony"));
	    FeedOther = ChatColor.translateAlternateColorCodes('&',message.getString("Message.FeedOther" , " zostal nakarmiony"));
	    
	    Fly = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Fly" , "Uzyj: /fly [on/off] <player>"));
	    FlyOn = ChatColor.translateAlternateColorCodes('&',message.getString("Message.FlyOn" , "Latanie wlaczone"));
	    FlyOff = ChatColor.translateAlternateColorCodes('&',message.getString("Message.FlyOff" , "Latanie wylaczone"));

	    FullLook = ChatColor.translateAlternateColorCodes('&',message.getString("Message.FullLook" ,"Musisz patrzec na pojemnik"));
	    Full = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Full" ,"Uzyj /full [ID:<Durability>]"));
	    FullWrong = ChatColor.translateAlternateColorCodes('&',message.getString("Message.FullWrong" ,"To nie jest pojemnik"));
	    FullGive = ChatColor.translateAlternateColorCodes('&',message.getString("Message.FullGive" ,"Uzupelniono blokiem:"));
	    
	    GM = ChatColor.translateAlternateColorCodes('&',message.getString("Message.GM" , "Uzyj: /gm [1/0] <player>"));
	    GMOn = ChatColor.translateAlternateColorCodes('&',message.getString("Message.GMOn" , "GameMode wlaczony"));
	    GMOff = ChatColor.translateAlternateColorCodes('&',message.getString("Message.GMOff" , "GameMode wylaczony"));
	   
	    Hat = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Hat" , "Masz nowe nakrycie glowy"));
	    HatEmpty = ChatColor.translateAlternateColorCodes('&',message.getString("Message.HatEmpty" , "Nie masz w rece przedmiotu"));
	    
	  	Heal = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Heal" , "Zostales uleczony ")); 
	  	HealOther = ChatColor.translateAlternateColorCodes('&',message.getString("Message.HealOther" , "Uleczyles gracza ")); 
	  	
	  	InvseeInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.InvseeInvalid" , "Uzyj /inv [player]"));
		
	  	Skull = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Skull" , "Otrzymales glowe"));
		SkullOther = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SkullOther" , " otrzymal glowe"));
		
	    More = ChatColor.translateAlternateColorCodes('&',message.getString("Message.More" , "Otrzymales 64 sztuki"));	    
	   
	    HelpOP = ChatColor.translateAlternateColorCodes('&',message.getString("Message.HelpOP" , "HelpOP" ));
	    HelpOPMsg = ChatColor.translateAlternateColorCodes('&',message.getString("Message.HelpOPMsg" , "Uzyj /hop [Wiadomosc]" ));
	    HelpOPColor = ChatColor.translateAlternateColorCodes('&',message.getString("Message.HelpOPColor" ,"" ));
	    
	    Item = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Item" , "Uzyj: /i [ID:<Durability>] <Amout>"));
	    ItemDurability = ChatColor.translateAlternateColorCodes('&',message.getString("Message.ItemDurability" , "Uzyj: /i [ID] lub /i [ID:Durability]"));
	    ItemAmout = ChatColor.translateAlternateColorCodes('&',message.getString("Message.ItemAmout" , "Uzyj: /i [ID] [amout] lub /i [ID:Durability] [amout]"));
	    Items = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Items" , "Otrzymales:"));
	    
	    PlayerTimeD = ChatColor.translateAlternateColorCodes('&',message.getString("Message.PlayerTimeD" , "Uzyj /dtime [wartosc]"));
	    PlayerTimeS = ChatColor.translateAlternateColorCodes('&',message.getString("Message.PlayerTimeS" ,"Uzyj /stime [wartosc]"));
	    DynamicTime = ChatColor.translateAlternateColorCodes('&',message.getString("Message.DynamicTime", "Dynamiczny czas Ustawiono na:"));
	    StaticTime = ChatColor.translateAlternateColorCodes('&',message.getString("Message.StaticTime" , "Statyczny czas Ustawiono na:"));
	    PlayerInvalidTime = ChatColor.translateAlternateColorCodes('&',message.getString("Message.PlayerInvalidTime" ,"Zla Wartossc"));
	    
	    Repair = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Repair" ,"Mozesz naprawiac tylko narzedzia i zbroje"));
	    RepairPass = ChatColor.translateAlternateColorCodes('&',message.getString("Message.RepairPass" ,"Przedmiot naprawiony"));	  
	    
	    Spawner = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Spawner" ,"Wpisz nazwe Moba!"));
	    SpawnerInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpawnerInvalid" ,"Zla nazwa Moba!"));
	    SpawnerLook = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpawnerLook" ,"Musisz patrzec na spawner"));
	    SpanwerWrong = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpanwerWrong" ,"To nie jest Spawner"));
	    SpawnerSet = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpawnerSet" ,"Spawner ustawiono na:"));
	    
	    Speed = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Speed" , "Uzyj: /speed [1-10]"));
	    SpeedInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpeedInvalid", "Zla predkosc "));
	    SpeedInfo = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpeedInfo" , "Szybkosc ustwiona na "));
	    
	    Tell = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Tell" , "Uzyj: /tell [player] [message]"));
		
	    Day = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Day" , "Ustawiono dzien na "));
	    DayAll = ChatColor.translateAlternateColorCodes('&',message.getString("Message.DayAll" , "Ustawiono dzien na wszystkich mapach."));
	    Night = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Night" , "Ustawiono noc na "));
	    NightAll = ChatColor.translateAlternateColorCodes('&',message.getString("Message.NightAll" , "Ustawiono dzien na wszystkich mapach."));
	    TimeColor = ChatColor.translateAlternateColorCodes('&',message.getString("Message.TimeColor" , ""));
	    
	    Troll = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Troll" , "Uzyj /troll [player] [Pozycja] <Swiat>"));
	   	   	  
	    VanishOn = ChatColor.translateAlternateColorCodes('&',message.getString("Message.VanishOn" , "Vanish wlaczony" ));
	    VanishOff = ChatColor.translateAlternateColorCodes('&',message.getString("Message.VanishOff" , "Vanish wylaczony"));
	    	   
	    Warps = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Warps" , "Wpisz nazwe warpu"));
	    WarpsInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WarpsInvalid" , " Taki warp nie istnieje"));
	    WarpsTP = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WarpsTP" , "Przeteleportowano do Warpu "));
	    WarpsSet = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WarpsSet", "Warp ustawiono "));
	    WarpsRemove = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WarpsRemove" , "Warp usunieto"));
	    WarpsInfo = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WarpsInfo", "Lista warpow: "));
	     
	    Sun = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Sun" , "Swieci slonce"));
	    Rain = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Rain" , "Pada deszcz"));
	    Thunder = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Thunder" , "Deszcz z piorunami"));
	    	   
	    WhoisInfo = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WhoisInfo" , "Nie podano gracza. Wyswietlam twoje statystyki"));
	    WhoisInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WhoisInvalid" , "Wpisz jednego gracza. Wyswietlam twoje statystyki"));
	    WhoisOfflnie = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WhoisOfflnie" , "Gracz nie jest Online"));
	    Whois = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Whois" , "Adres IP"));
	    
	    BroadcastColorMsg = ChatColor.translateAlternateColorCodes('&',message.getString("Message.BroadcastColorMsg" , ""));
	    BroadcastYT = ChatColor.translateAlternateColorCodes('&',message.getString("Message.BroadcastYT" ,"YouTuber" ));
	    BroadcastMsgYT = ChatColor.translateAlternateColorCodes('&',message.getString("Message.BroadcastMsgYT" , "Uzyj /yt [wiadomsoc]"));
	   
	    WorldInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.WorldInvalid" , "Podaj poprawny swiat")); 
	    Permissions = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Permissions", "You do not have permission!")); 
	    PlayerInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.PlayerInvalid" , "Nie znaleziono gracza: "));
  
	    Commands = message.getStringList("Commands");
	    
	    Price = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Price" ,"Cena"));
	    Money = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Money" ,"Nie masz pieniedzy na zmiane"));
	}
}