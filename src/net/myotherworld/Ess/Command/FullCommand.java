package net.myotherworld.Ess.Command;

import java.util.HashSet;

import net.myotherworld.Ess.Ess;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Dropper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class FullCommand implements CommandExecutor
{
	private Ess plugin;
	public FullCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.full")) 
		{	
			Player player = (Player) sender;
			if(args.length == 0)
			{
				player.sendMessage(plugin.messageData.Prefix + plugin.messageData.Full);
			} 
			else if(args.length == 1)
			{
				@SuppressWarnings("deprecation")
				//Patrzenie na skrzynie
				Block b = player.getTargetBlock((HashSet<Byte>)null, 100);
				if (!(b.getType() == Material.CHEST || b.getType() == Material.DISPENSER || b.getType() == Material.DROPPER)) 
				{
					player.sendMessage(plugin.messageData.Prefix + plugin.messageData.FullLook);
				}
				else 
				{
				    try 
				    {
				    	String[] idAndData = args[0].split(":");		
						int id = Integer.parseInt(idAndData[0]);	
						//Usupelnia Itemem "35" cala przestrzen
						if (idAndData.length == 1) 
						{							
							for(int i = 0; i <= 128; i++)
							{
								if(b.getType() == Material.CHEST)
								{
									Chest chest = (Chest) b.getState();
									@SuppressWarnings("deprecation")
									ItemStack IS = new ItemStack(id, i);
									chest.getInventory().addItem(IS);
								}								
								else if(b.getType() == Material.DISPENSER)
								{
									Dispenser dispenser = (Dispenser) b.getState();
									@SuppressWarnings("deprecation")
									ItemStack IS = new ItemStack(id, i);
									dispenser.getInventory().addItem(IS);
								}
								else if(b.getType() == Material.DROPPER)
								{
									Dropper dropper = (Dropper) b.getState();
									@SuppressWarnings("deprecation")
									ItemStack IS = new ItemStack(id, i);
									dropper.getInventory().addItem(IS);
								}															
							}													
							player.sendMessage(plugin.messageData.Prefix + plugin.messageData.FullGive + args[0]);	
						}
						else
						{
							int meta =  Integer.parseInt(idAndData[1]);
							//Usupelnia Itemem "35:1" cala skrzynie
							for(int i = 0; i <= 128; i++)
							{
								if(b.getType() == Material.CHEST)
								{
									Chest chest = (Chest) b.getState();
									@SuppressWarnings("deprecation")
									ItemStack IS = new ItemStack(id, 64, (short) meta);
									chest.getInventory().addItem(IS);
								}							
								else if(b.getType() == Material.DISPENSER)
								{
									Dispenser dispenser = (Dispenser) b.getState();
									@SuppressWarnings("deprecation")
									ItemStack IS = new ItemStack(id, 64, (short) meta);
									dispenser.getInventory().addItem(IS);
								}
								else if(b.getType() == Material.DROPPER)
								{
									Dropper dropper = (Dropper) b.getState();
									@SuppressWarnings("deprecation")
									ItemStack IS = new ItemStack(id, 64, (short) meta);
									dropper.getInventory().addItem(IS);
								}							
							}	
							player.sendMessage(plugin.messageData.Prefix + plugin.messageData.FullGive + args[0]);	
						}
		
				    }	
					catch (IllegalArgumentException e) 
					{
						player.sendMessage(plugin.messageData.Prefix + plugin.messageData.FullWrong);
					}	
				}
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}