package net.myotherworld.Ess.Command;

import java.util.ArrayList;

import net.myotherworld.Ess.Ess;
import net.myotherworld.Ess.Managers.WarpsManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpsCommand implements CommandExecutor 
{
	WarpsManager settings = WarpsManager.getInstance();
	
	private Ess plugin;
	public WarpsCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}        
            Player p = (Player) sender;
            
    		if(sender.hasPermission("MyOtherWorldEssentials.setwarp")) 
    		{	
    			if (cmd.getName().equalsIgnoreCase("setwarp")) 
    			{
    				if (args.length == 0) 
    				{
    					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Warps);
    					return true;
    				}
    				else
    				{
    					String warp = args[0].toUpperCase();
    					settings.getData().set("warps." + warp + ".world", p.getLocation().getWorld().getName());
    					settings.getData().set("warps." + warp + ".x", p.getLocation().getX());
    					settings.getData().set("warps." + warp + ".y", p.getLocation().getY());
    					settings.getData().set("warps." + warp + ".z", p.getLocation().getZ());
    					settings.getData().set("warps." + warp + ".Yaw", p.getLocation().getYaw());
    					settings.getData().set("warps." + warp + ".Pitch", p.getLocation().getPitch());
    					settings.saveData();
    					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.WarpsSet + args[0]);
    					return true;
    				}
    			}
    			
    		} 

    		if(sender.hasPermission("MyOtherWorldEssentials.warp")) 
    		{	
    			if (cmd.getName().equalsIgnoreCase("warp")) 
    			{
                    	if (args.length == 0) 
                    	{
                    		final ArrayList<String> warpNameList = new ArrayList<String>();
                    		warpNameList.addAll(settings.getData().getConfigurationSection("warps.").getKeys(false));
                    		p.sendMessage(plugin.messageData.Prefix + plugin.messageData.WarpsInfo);
                    		
                    		String warps = "";
                    		for(String warpName : warpNameList)
                    		{
                    			warps = warps + warpName + " ";
                    		}
                    		p.sendMessage(plugin.messageData.Prefix + warps);
                    		p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Warps);
                    		return true;
                    	}
                    	
                    	if (settings.getData().getConfigurationSection("warps." + args[0].toUpperCase()) == null) 
                    	{
                    		p.sendMessage(plugin.messageData.Prefix + args[0]  + plugin.messageData.WarpsInvalid);
                    		return true;
                    	}
                    	else
                    	{
                    		String warp = args[0].toUpperCase();
                    		if(p.hasPermission("MyOtherWorldEssentials.warp." + warp))
                    		{
	                    		World w = Bukkit.getServer().getWorld(settings.getData().getString("warps." + warp + ".world"));
	                    		double x = settings.getData().getDouble("warps." + warp + ".x");
	                    		double y = settings.getData().getDouble("warps." + warp + ".y");
	                    		double z = settings.getData().getDouble("warps." + warp + ".z");
	                    		float Yaw = settings.getData().getInt("warps." + warp + ".Yaw");
	                    		float Pitch = settings.getData().getInt("warps." + warp + ".Pitch");
	                    		p.teleport(new Location(w, x, y, z, Yaw, Pitch));
	                    		p.sendMessage(plugin.messageData.Prefix + plugin.messageData.WarpsTP + args[0]);
                    		}
                    		else p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
                    		return true;
                    	}
    			}
    		}
    		if(sender.hasPermission("MyOtherWorldEssentials.delwarp")) 
    		{	
    			if (cmd.getName().equalsIgnoreCase("delwarp")) 
    			{
    					if (args.length == 0) 
    					{
    						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Warps);
    						return true;
    					}
    					if (settings.getData().getConfigurationSection("warps." + args[0].toUpperCase()) == null) 
    					{
    						p.sendMessage(plugin.messageData.Prefix + args[0]  + plugin.messageData.WarpsInvalid);
    						return true;
    					}
                    		settings.getData().set("warps." + args[0].toUpperCase(), null);
                    		settings.saveData();
                    		p.sendMessage(plugin.messageData.Prefix + plugin.messageData.WarpsRemove + args[0]);
                    		return true;        
    			} 
    		}
    		else
    		{
    			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
    		}
			return true;   
    }
}