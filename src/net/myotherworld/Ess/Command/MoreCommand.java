package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MoreCommand implements CommandExecutor
{
	private Ess plugin;
	public MoreCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{		
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}	
		if(sender.hasPermission("MyOtherWorldEssentials.more")) 
		{	
			Player p = (Player)sender;
			if (args.length == 0) 
			{      			
				ItemStack IS = p.getItemInHand();
				int maxAmmountStack = IS.getMaxStackSize();
				IS.setAmount(maxAmmountStack);
				p.getInventory().setItemInHand(IS);
				sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.More);
				return true;
			}
		}		
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}