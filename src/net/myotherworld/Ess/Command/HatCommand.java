package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class HatCommand implements CommandExecutor 
{
	private Ess plugin;
	public HatCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldEssentials.hat")) 
		{	
		Player p = (Player)sender;
		
			if (args.length == 0)
			{					
				if (p.getItemInHand().getType() != Material.AIR)
				{
					ItemStack itemHand = p.getItemInHand();
					PlayerInventory inventory = p.getInventory();
					ItemStack itemHead = inventory.getHelmet();
					inventory.removeItem(new ItemStack[] { itemHand });
					inventory.setHelmet(itemHand);
					inventory.setItemInHand(itemHead);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Hat);
					return true;
				}	
				p.sendMessage(plugin.messageData.Prefix + plugin.messageData.HatEmpty);
				return true;							
			}
		}
	    else
	    {
	    	sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
	    }
		return true;	
	}
}