package net.myotherworld.Ess.Listener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import net.myotherworld.Ess.Ess;
import net.myotherworld.Ess.Command.ChatCommand;
import net.myotherworld.Ess.Command.VanishCommand;
import net.myotherworld.Ess.Data.CommandsData;
import net.myotherworld.Ess.Data.MessageData;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerListener implements Listener
{	
	private Ess plugin;
	public PlayerListener(Ess plugin)
	{
		this.plugin = plugin;
	}	
	@EventHandler(priority = EventPriority.LOW)
    public void onChat(AsyncPlayerChatEvent e) 
	{
        Player p = e.getPlayer();
        if (ChatCommand.chatDisabled) 
        {
        	if (!(p.hasPermission("MyOtherWorldEssentials.chat.bypass")))
        	{
        		e.setCancelled(true);
        		p.sendMessage(plugin.messageData.Prefix + plugin.messageData.ChatInfo);
        	}
        }
    }
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) 
	{
		for (Player p : VanishCommand.vanished) 
		{
			e.getPlayer().hidePlayer(p);
        }
	}
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) 
	{
		VanishCommand.vanished.remove(e.getPlayer());
	}
	@EventHandler
    public void onPowertoolClick(PlayerInteractEvent e) 
	{
		if ((((e.getAction() == Action.RIGHT_CLICK_AIR) || 
				(e.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
				e.getPlayer().getItemInHand().getType() == Material.STICK))	
		{		
			Player p = e.getPlayer();
	        ItemStack IS = p.getItemInHand();
	        ItemMeta Meta = IS.getItemMeta();
	        List<String> lore = Meta.getLore();
	        String Cmd = "";
	        if(lore != null)
	        {
		        for (String temp : lore)
		        {
		        	Cmd += temp;
		        }
		        Cmd = Cmd.substring(3);
		        p.performCommand(String.valueOf(Cmd));
	        }
		}  
	}
	public void onQuit (PlayerQuitEvent e) 
	{
		Player p = e.getPlayer();
		
		String Quit = plugin.messageData.Quit;
		Quit = Quit.replaceAll("%player", p.getPlayerListName());
		Quit = Quit.replaceAll("%server", plugin.messageData.Server);	
		e.setQuitMessage(Quit);
	}
	@EventHandler
	public void onJoin (PlayerJoinEvent e) 
	{
		Player p = e.getPlayer();
		
		String join = plugin.messageData.Join;
		join = join.replaceAll("%player", p.getPlayerListName());
		join = join.replaceAll("%server", plugin.messageData.Server);	
		e.setJoinMessage(join);
	}
    @EventHandler
    public void onMotd(PlayerJoinEvent event)
    {
    	File tempFile = new File(this.plugin.getDataFolder(), "motd.txt");
		if (!tempFile.exists()) 
			return;
		else
		{
			try(BufferedReader br = new BufferedReader(new FileReader(tempFile))) 
			{
				Player player = event.getPlayer();
			    for(String line; (line = br.readLine()) != null; ) 
			    {
			    	 String message = line.replace("{player}", player.getPlayerListName())
			            		.replace("{server}", plugin.messageData.Server)
			            		.replace("{online}", String.valueOf(Bukkit.getOnlinePlayers().size()));
			    	 
			    	 player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));	
			    }	          
			} 
			
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
    }
	@EventHandler
	public void onMove(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;		
    	}
    	if (event.getInventory().getName().equalsIgnoreCase(plugin.messageData.Prefix))
    	{
    		event.setCancelled(true);
    	}
    	if (event.getInventory().getName().equalsIgnoreCase(plugin.messageData.Server))
    	{
    		event.setCancelled(true);
    	} 
    	if (event.getInventory().getName().equalsIgnoreCase("�6Lista dostepnych mobow"))
    	{
    		event.setCancelled(true);
    	}   	
	}
	@EventHandler
	public void onClick(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;		
    	}
		try
		{	
			Player player = (Player) event.getWhoClicked();
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Reload message"))
	        {	
				plugin.messageData = new MessageData(plugin.fileManager.loadMessages());
				player.sendMessage("Poprawnie przeladowano message.yml");
	        }
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN +  "Reload config"))
	        {	
				plugin.commandsData = new CommandsData(plugin.fileManager.loadCommands());
				player.sendMessage("Poprawnie przeladowano config.yml");
	        }
		}
		catch (Exception localException) {}
	}
}
