package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MenuCommand implements CommandExecutor
{
	private Ess plugin;
	public MenuCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}		
    	if(sender.hasPermission("MyOtherWorldEssentials.menu")) 
    	{	
			if (args.length >= 0) 				
			{	
				Player player = (Player) sender;
				plugin.mainMenu.Menu(player);
				return true;			
			}
    	}
		return true;
	}
}
