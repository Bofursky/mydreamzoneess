package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TimeCommand implements CommandExecutor 
{
	private Ess plugin;
	public TimeCommand(Ess plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
    {
    	if (!(sender instanceof Player)) 
    	{
    		sender.sendMessage("Tylko gracz moze tego uzyc!");
    		return true;
    	}
    	Player p =(Player) sender;
		if(sender.hasPermission("MyOtherWorldEssentials.day")) 
		{	  
			if(cmd.getName().equalsIgnoreCase("day"))
			{
				if(args.length == 0)
				{
					p.getLocation().getWorld().setTime(1000);
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Day + plugin.messageData.TimeColor + p.getLocation().getWorld().getName());
				}
				else if(args.length == 1)
				{
					if(args[0].equalsIgnoreCase("all"))
					{
						for(org.bukkit.World world : Bukkit.getServer().getWorlds())
						{                  
							world.setTime(1000);
						}
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.DayAll);
					}
				}
			}
		}
		if(sender.hasPermission("MyOtherWorldEssentials.night")) 
		{	    	
    		if(cmd.getName().equalsIgnoreCase("night"))
    		{
    			if(args.length == 0)
    			{
    				p.getLocation().getWorld().setTime(20000);               
    				p.sendMessage(plugin.messageData.Prefix + plugin.messageData.Night + plugin.messageData.TimeColor + p.getLocation().getWorld().getName() + ".");          
    			}
    			else if(args.length == 1)
    			{
    				if(args[0].equalsIgnoreCase("all"))
    				{
    					for(org.bukkit.World world : Bukkit.getServer().getWorlds())
    					{
    						world.setTime(20000);
    					}
    					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.NightAll);
    				}
    			}
    		}
        	return true;
		}
    	else
    	{
    		sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
    	}
    	return true;
    }
}

