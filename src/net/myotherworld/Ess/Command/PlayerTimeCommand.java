package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerTimeCommand implements CommandExecutor
{
	private Ess plugin;
	public PlayerTimeCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{	
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		Player p = (Player)sender;
		if(sender.hasPermission("MyOtherWorldEssentials.stime")) 
		{									
			if(cmd.getName().equalsIgnoreCase("Statictime"))
			{
				if(args.length == 1)
				{
					try
					{
						int ptime = Integer.parseInt(args[0]);
						p.setPlayerTime(ptime, false);
						p.sendMessage(plugin.messageData.StaticTime + args[0]);
					}
					catch (IllegalArgumentException e) 
					{
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalidTime + args[0]);
					}					
				}
				else
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerTimeS);
					return true;
				}	
			}
		}		
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		if(sender.hasPermission("MyOtherWorldEssentials.dtime")) 
		{	
			if(cmd.getName().equalsIgnoreCase("Dynamictime"))
			{		
				if(args.length == 1)
				{
					try
					{
						int ptime = Integer.parseInt(args[0]);
						p.setPlayerTime(ptime, true);
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.DynamicTime + args[0]);
					}
					catch (IllegalArgumentException e) 
					{
						p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerInvalidTime + args[0]);
					}
				}
				else
				{
					p.sendMessage(plugin.messageData.Prefix + plugin.messageData.PlayerTimeD);
					return true;
				}
			}
		}		
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}