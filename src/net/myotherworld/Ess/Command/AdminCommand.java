package net.myotherworld.Ess.Command;

import net.myotherworld.Ess.Ess;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AdminCommand implements CommandExecutor
{
	private Ess plugin;
	public AdminCommand(Ess plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender.hasPermission("MyOtherWorldEssentials.admin")) 
		{	
			if (!(sender instanceof Player)) 
			{
				sender.sendMessage("Tylko gracz moze tego uzyc!");
				return true;
			}			
			if (args.length >= 0) 				
			{	
				if(sender.hasPermission("MyOtherWorldCase.settings")) 
				{
					Player player = (Player) sender;
					plugin.adminMenu.Menu(player);
					return true;
				}
			}
		}
		else
		{
			sender.sendMessage(plugin.messageData.Prefix + plugin.messageData.Permissions);
		}
		return true;
	}
}
